type Route = {
  text: string;
  path: string;
};

export type Routes = { [name: string]: Route };

export const routes: Routes = {
  offersList: {
    text: "Кто готов помочь",
    path: "/offers",
  },
  requestList: {
    text: "Кому нужна помощь",
    path: "/requests",
  },
  offer: {
    text: "Я хочу помочь",
    path: "/offer",
  },
  request: {
    text: "Мне нужна помощь",
    path: "/request",
  },
  // remove: {
  //   text: "Удалить",
  //   path: "/update",
  // },
};
