package com.hax0rs.meds.model.entity;

import com.hax0rs.meds.model.StatusType;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "medication_offer")
@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class MedicationOffer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String mobile;
    private String city;
    private String offer;
    private LocalDateTime createdAt;

    @Enumerated(EnumType.STRING)
    private StatusType status;

    private String reason;
}
