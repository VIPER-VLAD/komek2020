package com.hax0rs.meds.model.dto;

import com.hax0rs.meds.model.StatusType;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
@Validated
public class EditDTO {
    @ApiModelProperty(required = true)
    @NotNull
    private Long id;

    @ApiModelProperty(required = true)
    @NotNull
    private StatusType status;

    @ApiModelProperty(required = true)
    @NotNull
    @Size(min = 0, max = 40)
    private String reason;
}
